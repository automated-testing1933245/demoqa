package stepsdefinitions.elementssteps;

import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.junit.Assert;
import poms.elementspages.CheckBoxPage;

import static stepsdefinitions.Hooks.driver;

public class CheckBoxSteps {
    CheckBoxPage checkBoxPage;
    @Given("Be on Check Box page")
    public void beOnCheckBoxPage() {
        driver.get(CheckBoxPage.url);
        checkBoxPage = new CheckBoxPage(driver);
    }

    @When("Click on Home checkbox")
    public void clickOnHomeCheckbox() {
        checkBoxPage.clickOnHomeCheckbox();
    }

    @Then("Home checkbox is selected")
    public void homeCheckboxIsSelected() {
       Assert.assertTrue(checkBoxPage.homeCheckboxIsSelected());
    }
    @Then("General checkbox is selected")
    public void generalCheckboxIsSelected() {
        Assert.assertTrue(checkBoxPage.generalCheckboxIsSelected());
    }

    @And("home text is displayed in result")
    public void homeTextIsDisplayedInResult(){ Assert.assertTrue(checkBoxPage.homeResultIsDisplayed()); }

    @And("general text is displayed in result")
    public void generalTextIsDisplayedInResult(){ Assert.assertTrue(checkBoxPage.resultCheckboxesContain("general")); }

    @When("Click on Expand all button")
    public void clickOnExpandAllButton() { checkBoxPage.clickOnExpandAllButton(); }

    @Then("Checkboxes tree is expanded")
    public void checkboxesTreeIsExpanded() {
       Assert.assertTrue(checkBoxPage.allCheckboxesTreeIsExpanded());
    }

    @And("Click on Collapse all button")
    public void clickOnCollapseAllButton() { checkBoxPage.clickOnCollapseAllButton(); }

    @Then("Checkboxes tree is collapsed")
    public void checkboxesTreeIsCollapsed() { Assert.assertFalse(checkBoxPage.allCheckboxesTreeIsExpanded());}

    @And("Click on General checkbox")
    public void clickOnGeneralCheckbox() { checkBoxPage.clickOnGeneralCheckbox();}

    @And("Refresh the page")
    public void refreshThePage() {
        checkBoxPage.refresh();
    }
}
