package stepsdefinitions.elementssteps;

import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.junit.Assert;
import poms.elementspages.WebTablesPage;

import static stepsdefinitions.Hooks.driver;

public class WebTablesSteps {
    WebTablesPage webTablesPage;

    @Given("Be on Web Tables page")
    public void beOnTheWebTablesPage() {
        driver.get(WebTablesPage.url);
        webTablesPage = new WebTablesPage(driver);
    }

    @When("Click on Add button")
    public void clickOnAddButton() { webTablesPage.clickAddButton(); }

    @Then("Registration form is displayed")
    public void registrationFormIsDisplayed() {
        Assert.assertTrue("Registration form is not displayed", webTablesPage.registrationFormIsDisplayed());
    }

    @And("Click on Submit button")
    public void clickOnSubmitButton() { webTablesPage.clickSubmitButton(); }
}
