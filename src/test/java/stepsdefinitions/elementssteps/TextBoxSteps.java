package stepsdefinitions.elementssteps;

import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.junit.Assert;
import poms.elementspages.TextBoxPage;

import static stepsdefinitions.Hooks.driver;

public class TextBoxSteps {

    TextBoxPage textBoxPage;

    @Given("Be on Text Box page")
    public void beOnTextBoxPage() {
        driver.get(TextBoxPage.url);
        textBoxPage = new TextBoxPage(driver);
    }

    @When("Click Submit button")
    public void clickSubmitButton() {
        textBoxPage.clickSubmitButton();
    }

    @Then("Output text box is empty")
    public void outputTextBoxIsEmpty() {
        Assert.assertTrue("Output text is not empty", textBoxPage.outputTextIsEmpty());
    }

    @When("Enter Full Name {string}")
    public void enterFullName(String fullName) {
        textBoxPage.enterFullName(fullName);
    }

    @And("Enter Email {string}")
    public void enterEmail(String email) {
        textBoxPage.enterEmail(email);
    }

    @And("Enter Current Address {string}")
    public void enterCurrentAddress(String currentAddress) {
        textBoxPage.enterCurrentAddress(currentAddress);
    }

    @And("Enter Permanent Address {string}")
    public void enterPermanentAddress(String permanentAddress) {
        textBoxPage.enterPermanentAddress(permanentAddress);
    }

    @Then("Output box displays data from Input")
    public void outputBoxDisplaysDataFromInput() {
        Assert.assertFalse("Text output box is not displayed", textBoxPage.outputTextIsEmpty());
        Assert.assertEquals("Output name doesn't match the input name",
                "Name:Aria Bennett", textBoxPage.getOutputName());
        System.out.println(textBoxPage.getOutputName());
        Assert.assertEquals("Output email doesn't match the input email",
                "Email:testemail@example.com", textBoxPage.getOutputEmail());
        System.out.println(textBoxPage.getOutputEmail());
        Assert.assertEquals("Output current address doesn't match the input current address",
                "Current Address :123 Maple Street, Evergreen Meadows, Azure City, AC 56789, Wonderland", textBoxPage.getOutputCurrentAddress());
        System.out.println(textBoxPage.getOutputCurrentAddress());

        //The following step should fail because "Permananent" has a spelling error
        Assert.assertEquals("Output permanent address doesn't match the input permanent address",
                "Permananet Address :456 Sunflower Avenue, Serenity Springs, Cobalt County, CC 12345, Dreamland", textBoxPage.getOutputPermanentAddress());
        System.out.println(textBoxPage.getOutputPermanentAddress());
    }

    @When("Enter invalid email {string}")
    public void enterInvalidEmail(String invalidEmail) {
        textBoxPage.enterEmail(invalidEmail);
    }

    @When("Email missing @ symbol tooltip message is displayed")
    public void emailMissingAtSymbolTooltipMessageDisplayed() {
        Assert.assertEquals("what ",textBoxPage.getEmailTexboxInputTooltipText());
    }

    @When("Part following @ symbol missing tooltip message is displayed")
    public void partFollowingAtSymbolMissingTooltipMessageDisplayed() {

    }

    @And("Hover cursor over email text box input")
    public void hoverCursorOverEmailTextBoxInput() {
        textBoxPage.hoverCursorOverEmailTextBoxInput();
    }

}
