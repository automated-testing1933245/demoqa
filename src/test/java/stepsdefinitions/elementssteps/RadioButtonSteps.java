package stepsdefinitions.elementssteps;

import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.junit.Assert;
import poms.elementspages.RadioButtonPage;

import static stepsdefinitions.Hooks.driver;

public class RadioButtonSteps {
    RadioButtonPage radioButtonPage;
    @Given("Be on Radio Button page")
    public void beOnRadioButtonPage() {
        driver.get(RadioButtonPage.url);
        radioButtonPage = new RadioButtonPage(driver);
    }

    @When("Click on the {string} radio button")
    public void clickOnTheYesButton(String radioButton) { radioButtonPage.clickOnRadioButton(radioButton); }

    @Then("{string} radio button is selected")
    public void yesRadioButtonIsSelected(String radioButton) {
        Assert.assertTrue("Radio button " + radioButton + " is not selected",radioButtonPage.radioButtonIsSelected(radioButton)); }

    @And("You have selected {string} is displayed")
    public void youHaveSelectedIsDisplayed(String radioButtonText) {
        Assert.assertTrue("Radio button text " + radioButtonText + " is not displayed", radioButtonPage.radioButtonTextIsDisplayed(radioButtonText));
    }

    @Then("'No' radio Button is disabled")
    public void noRadioButtonIsDisabled() {
        Assert.assertFalse("'No' radio button is enabled", radioButtonPage.isNoRadioButtonDisabled());
    }
}
