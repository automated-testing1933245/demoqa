package stepsdefinitions.bookstoreapplicationsteps;

import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import net.bytebuddy.utility.RandomString;
import org.junit.Assert;
import org.openqa.selenium.Alert;
import org.openqa.selenium.support.ui.ExpectedConditions;
import poms.bookstorepages.RegisterPage;

import static stepsdefinitions.Hooks.driver;

public class RegisterSteps {
    RegisterPage registerPage;

    @Given("Be on Register page")
    public void beOnRegisterPage(){
        driver.get(RegisterPage.url);
        registerPage = new RegisterPage(driver);
    }

    @When("Enter First Name {string}")
    public void enterFirstName(String firstName) {
        String randomFirstName = RandomString.make();
        System.out.println("First name is: " + randomFirstName );
        registerPage.enterFirstName(randomFirstName);

    }

    @When("Enter Last Name {string}")
    public void enterLastName(String lastName) {
        String randomLastName = RandomString.make();
        System.out.println("Last name is: " + randomLastName );
        registerPage.enterLastName(randomLastName);
    }

    @And("Enter UserName {string}")
    public void enterUserName(String username) {
        String randomUsername = RandomString.make();
        System.out.println("Username is: " + randomUsername );
        registerPage.enterUserName(randomUsername);
    }

    @And("Enter Password {string}")
    public void enterPassword(String password) {
        String randomPassword = RandomString.make();
        System.out.println("Password is: " + randomPassword + "aA1!");
        registerPage.enterPassword(randomPassword + "aA1!");
    }

    @And("Check ReCaptcha")
    public void checkReCaptcha() {
       //registerPage.wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector("#registerPage.switchToReCaptchaIFrame();\n")));
       //registerPage.switchToReCaptchaIFrame();
        System.out.println("I'm here 1");
       registerPage.checkCaptchaCheckbox();
        System.out.println("I'm here 2");
       driver.switchTo().defaultContent();
        System.out.println("I'm here 3");

    }

    @When("Click Register button")
    public void clickRegisterButton() {
        System.out.println("I will click the register button");
        registerPage.clickRegisterButton();
        System.out.println("we are here");
    }

    @Then("Alert confirming successful user registration is displayed")
    public void alertConfirmingSuccessfulUserRegistrationIsDisplayed() {
        Alert alert = null;
        registerPage.wait.until(ExpectedConditions.alertIsPresent());
        driver.switchTo().alert();
        Assert.assertEquals("User register successfully alert is not displayed",
                    "User Register Successfully.",
                              alert.getText());
        driver.switchTo().defaultContent();
    }

    @Then("Exclamation mark and tooltip message is displayed")
    public void exclamationMarkAndTooltipMessageIsDisplayed() {
    }

    @Then("Captcha error message is displayed")
    public void captchaErrorMessageIsDisplayed() {
    }

    @Then("Password invalid format error message is displayed")
    public void passwordInvalidFormatErrorMessageIsDisplayed() {
    }
}
