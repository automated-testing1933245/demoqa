package stepsdefinitions.bookstoreapplicationsteps;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.junit.Assert;
import poms.HomePage;
import poms.alertsframewindowspages.AlertsFrameWindowsPage;
import poms.bookstorepages.BookStorePage;
import poms.elementspages.ElementsPage;
import poms.formspages.FormsPage;
import poms.interactionspages.InteractionsPage;
import poms.widgetspages.WidgetsPage;

import static stepsdefinitions.Hooks.driver;

public class NavigateFromHomePageSteps {
    HomePage homePage;
    ElementsPage elementsPage;
    FormsPage formsPage;
    AlertsFrameWindowsPage alertsFrameWindowsPage;
    WidgetsPage widgetsPage;
    InteractionsPage interactionsPage;
    BookStorePage bookStorePage;

    @Given("Be on Home page")
    public void beOnHomePage() {
        driver.get(HomePage.url);
        homePage = new HomePage(driver);
    }

    @When("Click on Elements button")
    public void clickOnElementsButton() {
        homePage.clickElementsButton();
        elementsPage = new ElementsPage(driver);
    }

    @Then("Navigate to Elements page")
    public void navigateToElementsPage() {
        Assert.assertEquals("The URL is not " + ElementsPage.url, ElementsPage.url, driver.getCurrentUrl());
        Assert.assertEquals("The page title is not " + ElementsPage.title, ElementsPage.title, elementsPage.getPageTitle());
    }

    @When("Click on Forms button")
    public void clickOnFormsButton() {
        homePage.clickFormsButton();
        formsPage= new FormsPage(driver);
    }

    @Then("Navigate to Forms page")
    public void navigateToFormsPage() {
        Assert.assertEquals("The URL is not " + FormsPage.url, FormsPage.url, driver.getCurrentUrl());
        Assert.assertEquals("The page title is not " + FormsPage.title, FormsPage.title, formsPage.getPageTitle());
    }

    @When("Click on Alerts, Frame & Windows button")
    public void clickOnAlertsFrameWindowsButton() {
        homePage.clickAlertsFramesAndWindowsButton();
        alertsFrameWindowsPage = new AlertsFrameWindowsPage(driver);
    }

    @Then("Navigate to Alerts, Frame & Windows page")
    public void navigateToAlertsFrameWindowsPage() {
        Assert.assertEquals("The URL is not " + AlertsFrameWindowsPage.url, AlertsFrameWindowsPage.url, driver.getCurrentUrl());
        Assert.assertEquals("The page title is not " + AlertsFrameWindowsPage.title, AlertsFrameWindowsPage.title, alertsFrameWindowsPage.getPageTitle());
    }

    @When("Click on Widgets button")
    public void clickOnWidgetsButton() {
        homePage.clickWidgetsButton();
        widgetsPage = new WidgetsPage(driver);
    }

    @Then("Navigate to Widgets page")
    public void navigateToWidgetsPage() {
        Assert.assertEquals("The URL is not " + WidgetsPage.url, WidgetsPage.url, driver.getCurrentUrl());
        Assert.assertEquals("The page title is not " + WidgetsPage.title, WidgetsPage.title, widgetsPage.getPageTitle());
    }

    @When("Click on Interactions button")
    public void clickOnInteractionsButton() {
        homePage.clickInteractionsButton();
        interactionsPage = new InteractionsPage(driver);
    }

    @Then("Navigate to Interactions page")
    public void navigateToInteractionsPage() {
        Assert.assertEquals("The URL is not " + InteractionsPage.url, InteractionsPage.url, driver.getCurrentUrl());
        Assert.assertEquals("The page title is not " + InteractionsPage.title, InteractionsPage.title, interactionsPage.getPageTitle());
    }

    @When("Click on Book Store Application button")
    public void clickOnBookStoreApplicationButton() {
        homePage.clickBookStoreApplicationButton();
        bookStorePage = new BookStorePage(driver);
    }

    @Then("Navigate to Book Store Application Page")
    public void navigateToBookStoreApplicationPage() {
        Assert.assertEquals("The URL is not " + BookStorePage.url, BookStorePage.url, driver.getCurrentUrl());
        Assert.assertEquals("The page title is not " + BookStorePage.title, BookStorePage.title, bookStorePage.getPageTitle());
    }
}
