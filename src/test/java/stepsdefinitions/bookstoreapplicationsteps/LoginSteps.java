package stepsdefinitions.bookstoreapplicationsteps;

import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.junit.Assert;
import org.openqa.selenium.support.ui.ExpectedConditions;
import poms.bookstorepages.BookStorePage;
import poms.bookstorepages.LoginPage;
import poms.bookstorepages.ProfilePage;
import static stepsdefinitions.Hooks.driver;

public class LoginSteps {
    LoginPage loginPage;
    ProfilePage profilePage;
    BookStorePage bookStorePage;

    @Given("Be on Book Store page")
    public void beOnBookStorePage() {
        driver.get(BookStorePage.url);
        bookStorePage = new BookStorePage(driver);
    }

    @When("Click Login button above books table")
    public void clickLoginButtonAboveBooksTable() {
        loginPage = new LoginPage(driver);
        bookStorePage.clickTableLoginButton();}

    @When("Click Login button from Book Store Application dropdown")
    public void clickLoginButtonFromBookStoreApplicationDropdown() {
        bookStorePage.clickDropDownLoginButton();
        loginPage = new LoginPage(driver);
    }

    @And("Click Login button")
    public void clickLoginButton() {
        loginPage.clickLoginButton();
        profilePage = new ProfilePage(driver);
    }

    @When("Enter username {string}")
    public void enterUsername(String username) {
        loginPage.enterUsername(username);
    }

    @When("Enter password {string}")
    public void enterPassword(String password) { loginPage.enterPassword(password); }

    @Then("Error message is displayed")
    public void errorMessageIsDisplayed() {
        profilePage.wait.until(ExpectedConditions.visibilityOfElementLocated(loginPage.errorMessageParagraph));
        Assert.assertTrue(loginPage.errorMessageIsDisplayed()); }

    @Then("Please fill out this field for username is displayed")
    public void pleaseFillOutThisFieldForUsernameIsDisplayed() {
        Assert.assertTrue("Please fill out this field for username is not displayed",
                loginPage.usernameInputIsEmptyIsDisplayed());
    }

    @Then("Please fill out this field for password is displayed")
    public void pleaseFillOutThisFieldForPasswordIsDisplayed() {
        Assert.assertTrue("Please fill out this field for password is not displayed",
                loginPage.passwordInputIsEmptyIsDisplayed());
    }

    @Then("Ensure user is on Book Store page")
    public void ensureUserIsOnBookStorePage(){
        profilePage.wait.until(ExpectedConditions.visibilityOfElementLocated(bookStorePage.loggedInUserName));
        Assert.assertEquals("Page title is not Book Store",BookStorePage.title, bookStorePage.getPageTitle());
    }

    @Then("Ensure user is on Profile page")
    public void ensureUserIsOnProfilePage() {
        profilePage.wait.until(ExpectedConditions.visibilityOfElementLocated(profilePage.loggedInUserName));
        Assert.assertEquals("Page title is not Profile", profilePage.title, profilePage.getPageTitle());
    }

    @Then("Username is {string}")
    public void usernameIs(String username) {
        Assert.assertEquals("Username is not displayed",
                username, profilePage.getLoggedInUserName());
    }

    @And("Logout button is displayed")
    public void logoutButtonIsDisplayed() {
        profilePage.wait.until(ExpectedConditions.visibilityOfElementLocated(profilePage.logoutButton));
        Assert.assertTrue(profilePage.logOutButtonIsDisplayed());
    }

    @Given("Be on Login page")
    public void beOnLoginPage() {
        driver.get(LoginPage.url);
        loginPage = new LoginPage(driver);
    }

    @Then("Logout button is not displayed")
    public void logoutButtonIsNotDisplayed() {
        Assert.assertTrue(bookStorePage.logoutButtonCannotBeFound());
    }
}
