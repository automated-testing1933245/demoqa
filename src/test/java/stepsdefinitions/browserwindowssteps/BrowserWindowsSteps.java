package stepsdefinitions.browserwindowssteps;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.junit.Assert;
import poms.alertsframewindowspages.BrowserWindowsPage;
import static stepsdefinitions.Hooks.driver;

public class BrowserWindowsSteps {
    BrowserWindowsPage browserWindowsPage;
    @Given("Be on Browser Windows page")
    public void beOnCheckBoxPage() {
        driver.get(BrowserWindowsPage.url);
        browserWindowsPage = new BrowserWindowsPage(driver);
    }

    @When("Click on New Tab button")
    public void clickOnNewTabButton()  { browserWindowsPage.clickOnNewTabButton(); }

    @Then("New tab opens and Page title is This is a sample page")
    public void newTabOpensAndPageTitleIsThisIsASimplePage() {
        browserWindowsPage.navigateToNewOpenedTabOrWindow();
        Assert.assertEquals("Page title is not 'This is a sample page' ", "This is a sample page", browserWindowsPage.getNewTabPageTitle());
    }

    @When("Click on New Window button")
    public void clickOnNewWindowButton() { browserWindowsPage.clickOnNewWindowButton(); }

    @Then("New window opens and Page title is This is a sample page")
    public void newWindowOpensAndPageTitleIsThisIsASamplePage() {
        browserWindowsPage.navigateToNewOpenedTabOrWindow();
        Assert.assertEquals("Page title is not 'This is a sample page' ", "This is a sample page", browserWindowsPage.getNewTabPageTitle());
    }
}
