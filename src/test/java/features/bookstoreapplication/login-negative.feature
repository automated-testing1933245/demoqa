@UI @Login @Negative
Feature: Login negative test scenarios

  # Negative scenario
  Scenario: Login without entering username and password
    Given Be on Login page

    When Click Login button
    Then Please fill out this field for username is displayed
    And Please fill out this field for password is displayed

  # Negative scenario
  Scenario Outline: Login by entering username without password
    Given Be on Login page

    When Enter username "<username>"
    And  Click Login button
    Then Please fill out this field for password is displayed
    Examples:
      | username          |
      | CorrectUsername   |
      | IncorrectUsername |
      | !@#$%^&*()        |

  # Negative scenario
  Scenario Outline: Login by entering password without username
    Given Be on Login page

    When Enter password "<password>"
    And  Click Login button
    Then Please fill out this field for username is displayed

    Examples:
      | password          |
      | CorrectPassword1! |
      | IncorrectPassword |
      | dfsf4#$%rr        |

  # Negative scenario
  Scenario: Login by entering correct username and wrong password
    Given Be on Login page

    When Enter username "CorrectUsername"
    And  Enter password "WrongPassword"
    And  Click Login button
    Then Error message is displayed

  # Negative scenario
  Scenario: Login by entering correct password and wrong username
    Given Be on Login page

    When Enter username "WrongUsername"
    And  Enter password "CorrectPassword1!"
    And  Click Login button
    Then Error message is displayed

  # Negative scenario
  Scenario Outline: Login with wrong username and password
    Given Be on Login page

    When Enter username "<wrongUsername>"
    And  Enter password "<wrongPassword>"
    And  Click Login button
    Then Error message is displayed

    Examples:
      | wrongUsername   | wrongPassword   |
      | InvalidUsername | InvalidPassword |
      | !@#$%^&*()      | !@#$%^&*()      |
      | tweydb23423d    | sdfsfdsf34r535  |













