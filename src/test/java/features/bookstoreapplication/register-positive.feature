@UI @Register @Positive
Feature: Register positive test scenarios

  # Positive scenario
  Scenario: Register with all correct fields
    Given Be on Register page

    When Enter First Name "<FirstName>"
    And  Enter Last Name "<LastName>"
    And  Enter UserName "<Username>"
    And  Enter Password "<Password>"
    And  Check ReCaptcha
    And  Click Register button
    Then Alert confirming successful user registration is displayed


