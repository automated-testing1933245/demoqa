@UI @Register @Negative

Feature: Register negative test scenarios

  # Negative scenario
  Scenario: Register without entering any fields
    Given Be on Register page

    When Click Register button
    Then Exclamation mark and tooltip message is displayed

  # Negative scenario
  Scenario: Register without entering First name
    Given Be on Register page

    When Enter Last Name "<LastName>"
    And  Enter UserName "<Username>"
    And  Enter Password "<Password>"
    And  Check ReCaptcha
    And  Click Register button
    Then Exclamation mark and tooltip message is displayed

  # Negative scenario
  Scenario: Register without entering Last name
    Given Be on Register page

    When Enter First Name "<FirstName>"
    And  Enter UserName "<Username>"
    And  Enter Password "<Password>"
    And  Check ReCaptcha
    And  Click Register button
    Then Exclamation mark and tooltip message is displayed

  # Negative scenario
  Scenario: Register without entering UserName
    Given Be on Register page

    When Enter First Name "<FirstName>"
    And  Enter Last Name "<LastName>"
    And  Enter Password "<Password>"
    And  Check ReCaptcha
    And  Click Register button
    Then Exclamation mark and tooltip message is displayed

  # Negative scenario
  Scenario: Register without entering Password
    Given Be on Register page

    When Enter First Name "<FirstName>"
    And  Enter Last Name "<LastName>"
    And  Enter UserName "<Username>"
    And  Check ReCaptcha
    And  Click Register button
    Then Exclamation mark and tooltip message is displayed

    # Negative scenario
  Scenario: Register without verifying captcha Password
    Given Be on Register page

    When Enter First Name "<FirstName>"
    And  Enter Last Name "<LastName>"
    And  Enter UserName "<Username>"
    And  Check ReCaptcha
    And  Click Register button
    Then Captcha error message is displayed

    # Negative scenario
  Scenario: Register with invalid Password
    Given Be on Register page

    When Enter First Name "<FirstName>"
    And  Enter Last Name "<LastName>"
    And  Enter UserName "<Username>"
    And  Check ReCaptcha
    And  Click Register button
    Then Password invalid format error message is displayed