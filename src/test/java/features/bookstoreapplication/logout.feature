@UI @Logout
  Feature: Logout test scenario

    @Negative
    Scenario: When the user is not logged in the Logout button is not displayed
      Given Be on Book Store page

      Then Logout button is not displayed
