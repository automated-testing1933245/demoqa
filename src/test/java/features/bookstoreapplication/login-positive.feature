@UI @Login @Positive
Feature: Login positive test scenarios

  # Positive scenario
  Scenario: Login with correct username and password
    Given Be on Login page

    When Enter username "correctUsername"
    And  Enter password "correctPassword1!"
    And  Click Login button
    Then Logout button is displayed

  # Positive scenario
  Scenario Outline: Login from button above books table with correct credentials multiple examples
    Given Be on Book Store page

    When Click Login button above books table
    And  Enter username "<correctUsername>"
    And  Enter password "<correctPassword>"
    And  Click Login button
    Then Ensure user is on Book Store page
    And  Username is "<correctUsername>"
    And  Logout button is displayed

    Examples:
      | correctUsername | correctPassword   |
      | CorrectUsername | correctPassword1! |
      | ValidUsername   | validPassword1!   |

  # Positive scenario
  Scenario Outline: Login from Book Store dropdown button with correct credentials multiple examples
    Given Be on Book Store page

    When Click Login button from Book Store Application dropdown
    And  Enter username "<correctUsername>"
    And  Enter password "<correctPassword>"
    And  Click Login button
    Then Ensure user is on Profile page
    And  Username is "<correctUsername>"
    And  Logout button is displayed

    Examples:
      | correctUsername | correctPassword   |
      | CorrectUsername | correctPassword1! |
      | ValidUsername   | validPassword1!   |
