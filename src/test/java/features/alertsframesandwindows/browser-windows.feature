@UI @BrowserWindows
  Feature: Browser Windows page tests

    @Positive
    Scenario: Click on New Tab button and new tab opens
      Given Be on Browser Windows page

      When Click on New Tab button
      Then New tab opens and Page title is This is a sample page


    @Positive
    Scenario: Click on New Window button and new window opens
      Given Be on Browser Windows page

      When Click on New Window button
      Then New window opens and Page title is This is a sample page
