@UI @RadioButton @Positive
Feature: Radio Button page tests

  Scenario Outline: Click Yes/Impressive radio button and have Yes/Impressive displayed
    Given Be on Radio Button page

    When  Click on the "<radioButton>" radio button
    Then "<radioButton>" radio button is selected
    And  You have selected "<radioButton>" is displayed
    Examples:
      | radioButton |
      | Yes         |
      | Impressive  |

    Scenario: 'No' radio button is disabled
      Given Be on Radio Button page

      Then 'No' radio Button is disabled