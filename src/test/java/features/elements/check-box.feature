@UI @CheckBox @Positive
Feature: Check Box tests

  Scenario: Checkboxes tree is collapsed after refresh
    Given Be on Check Box page

    When Click on Expand all button
    And  Refresh the page
    Then Checkboxes tree is collapsed

  Scenario: Expand all checkboxes tree
    Given Be on Check Box page

    When Click on Expand all button
    Then Checkboxes tree is expanded

  Scenario: Collapse all checkboxes tree
    Given Be on Check Box page

    When Click on Expand all button
    And  Click on Collapse all button
    Then Checkboxes tree is collapsed

  Scenario: Check the General checkbox
    Given Be on Check Box page

    When Click on Expand all button
    And  Click on General checkbox
    Then General checkbox is selected
    And  general text is displayed in result

  Scenario: Check the Home checkbox
    Given Be on Check Box page

    When Click on Home checkbox
    Then Home checkbox is selected
    And  home text is displayed in result







