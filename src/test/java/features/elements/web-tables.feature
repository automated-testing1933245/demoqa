@UI @WebTables @Positive
Feature: Web Tables page tests

  Scenario: Registration form displayed when clicking on Add button
    Given Be on Web Tables page

    When  Click on Add button
    Then  Registration form is displayed

  Scenario: Registration form displayed when clicking on Add button
    Given Be on Web Tables page

    When  Click on Add button
    And   Click on Submit button
    Then  Registration form is displayed

