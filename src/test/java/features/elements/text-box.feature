@UI @TextBox
Feature: Text Box page tests

   @Positive
  Scenario: Click Submit with all fields empty
    Given Be on Text Box page

    When Click Submit button
    Then Output text box is empty

  @Positive
  Scenario: Fill in all text box fields with valid data
    Given Be on Text Box page

    When Enter Full Name "Aria Bennett"
    And  Enter Email "testemail@example.com"
    And  Enter Current Address "123 Maple Street, Evergreen Meadows, Azure City, AC 56789, Wonderland"
    And  Enter Permanent Address "456 Sunflower Avenue, Serenity Springs, Cobalt County, CC 12345, Dreamland"
    And  Click Submit button
    Then Output box displays data from Input

  @Exclude
  @Negative
  Scenario: Enter invalid emails without @
    Given Be on Text Box page

    When Enter invalid email "emailmissingat.com"
    And  Hover cursor over email text box input
    Then Email missing @ symbol tooltip message is displayed

  @Exclude
  @Negative
  Scenario: Enter invalid email without domain
    Given Be on Text Box page

    When Enter invalid email "emailmissingdomain@"
    Then Part following @ symbol missing tooltip message is displayed


