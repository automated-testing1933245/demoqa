@UI @Home @Positive
Feature: Navigate to all pages from Home page

  Scenario: Navigate to Elements page from Home page
    Given Be on Home page

    When Click on Elements button
    Then Navigate to Elements page

  Scenario: Navigate to Forms page from Home page
    Given Be on Home page

    When Click on Forms button
    Then Navigate to Forms page

  Scenario: Navigate to Alerts, Frame & Windows page from Home page
    Given Be on Home page

    When Click on Alerts, Frame & Windows button
    Then Navigate to Alerts, Frame & Windows page

  Scenario: Navigate to Widgets page from Home page
    Given Be on Home page

    When Click on Widgets button
    Then Navigate to Widgets page

  Scenario: Navigate to Interactions page from Home page
    Given Be on Home page

    When Click on Interactions button
    Then Navigate to Interactions page

  Scenario: Navigate to Book Store Application page from Home page
    Given Be on Home page

    When Click on Book Store Application button
    Then Navigate to Book Store Application Page

