package runners;

import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions(
        features = "src/test/java/features",
        glue = "stepsdefinitions",
        tags = "@UI and not @Exclude",
        plugin = {"pretty", "html:target/UITests.html"}
)
public class UITestsRunner {
}
