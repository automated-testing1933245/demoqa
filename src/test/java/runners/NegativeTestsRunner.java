package runners;

import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions(
        features = "src/test/java/features/bookstoreapplication",
        glue = "stepsdefinitions",
        tags = "@Negative",
        plugin = {"pretty", "html:target/NegativeTests.html"}
)

public class NegativeTestsRunner {
}
