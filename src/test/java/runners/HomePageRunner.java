package runners;

import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions(
        features = "src/test/java/features/home",
        glue = "stepsdefinitions",
        tags = "@Home and not @Exclude",
        plugin = {"pretty", "html:target/LoginTests.html"}
)

public class HomePageRunner {
}
