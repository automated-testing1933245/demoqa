package runners;

import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions(
        features = "src/test/java/features/bookstoreapplication",
        glue = "stepsdefinitions",
        tags = "not @Exclude and @Register",
        plugin = {"pretty", "html:target/RegisterTests.html"}
)
public class RegisterTestsRunner {
}
