package runners;

import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions(
        features = "src/test/java/features/bookstoreapplication",
        glue = "stepsdefinitions",
        tags = "@Logout and not @Exclude",
        plugin = {"pretty", "html:target/LogoutTests.html"}
)
public class LogoutTestsRunner {
}
