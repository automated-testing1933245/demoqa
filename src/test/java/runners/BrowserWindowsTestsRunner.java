package runners;

import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions(
        features = "src/test/java/features/alertsframesandwindows",
        glue = "stepsdefinitions",
        tags = "@BrowserWindows and not @Exclude",
        plugin = {"pretty", "html:target/BrowserWindowsTests.html"}
)
public class BrowserWindowsTestsRunner {
}
