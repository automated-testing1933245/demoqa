package runners;
import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions(
        features = "src/test/java/features/elements",
        glue = "stepsdefinitions",
        tags = "@CheckBox",
        plugin = {"pretty", "html:target/CheckBoxTests.html"}
)
public class CheckBoxPageRunner {
}
