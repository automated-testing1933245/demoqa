package models.webtables;

public class RegistrationFormBuilder {
    String firstName = "Ion";
    String lastName = "Vasilescu";
    String email = "ion@email.com";
    String age = "23";
    String salary = "5000";
    String department = "Maintenance";

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setAge(String age) {
        this.age = age;
    }

    public void setSalary(String salary) {
        this.salary = salary;
    }

    public void setDepartment(String department) {
        this.department = department;
    }
}
