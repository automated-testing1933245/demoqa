package models;

public class UserInfoBuilder {
    String firstName ="";
    String lastName = "";
    String userName = "";
    String password = "";

    public UserInfo build() {
        return new UserInfo(firstName, lastName, userName, password);
    }

    public UserInfoBuilder setfirstName(String firstName) {
        this.firstName = firstName;
        return this;
    }
    public UserInfoBuilder setlastName(String lastName) {
        this.lastName = lastName;
        return this;
    }
    public UserInfoBuilder setuserName(String userName) {
        this.userName = userName;
        return this;
    }
    public UserInfoBuilder setpassword(String password) {
        this.password = password;
        return this;
    }

}
