package models;

public class UserInfo {
    String firstName;
    String lastName;
    String userName;
    String password;

    public UserInfo(String firstName, String lastName, String userName, String password){
        this.firstName = firstName;
        this.lastName = lastName;
        this.userName = userName;
        this.password = password;
    }

    public String getFirstName() { return firstName;}
    public String getLastName() { return lastName; }
    public String getUserName() { return userName; }
    public String getPassword() { return password; }
}
