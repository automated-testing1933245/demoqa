package poms.alertsframewindowspages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import poms.BasePage;

import java.util.Set;

public class BrowserWindowsPage extends BasePage {

    public static final String url = "https://demoqa.com/browser-windows";
    public static final String title= "Browser Windows";

    //constructor
    public BrowserWindowsPage(WebDriver driver) {
        super(driver);
    }

    //selectors
    public By newTabButton = By.id("tabButton");
    public By newWindowButton = By.id("windowButton");
    public By newWindowMessageButton = By.id("messageWindowButton");
    public By newTabOrWindowPageTitle = By.id("sampleHeading");

    //actions
    public void clickOnNewTabButton (){ driver.findElement(newTabButton).click(); }

    public void navigateToNewOpenedTabOrWindow (){
        Set<String> windowHandles = driver.getWindowHandles();
        for (String handle : windowHandles) {
            driver.switchTo().window(handle);
        }
    }
    public void clickOnNewWindowButton (){ driver.findElement(newWindowButton).click(); }

    public void clickOnNewWindowMessageButton (){ driver.findElement(newWindowMessageButton).click(); }

    //verifications

    public String getNewTabPageTitle () { return driver.findElement(newTabOrWindowPageTitle).getText(); }

}
