package poms.alertsframewindowspages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import poms.BasePage;

public class AlertsFrameWindowsPage extends BasePage {

    public static String url = "https://demoqa.com/alertsWindows";
    public static final String title = "Alerts, Frame & Windows";


    //selectors
    public By browserWindowsButton = By.xpath("//*[@id='item-0'][.//span[@class='text' and contains(text(),'Browser Windows')]]");
    public By alertsButton = By.xpath("//*[@id='item-1'][.//span[@class='text' and contains(text(),'Alerts')]]");
    public By framesButton = By.xpath("//*[@id='item-2'][.//span[@class='text' and contains(text(),'Frames')]]");
    public By nestedFramesButton = By.xpath("//*[@id='item-3'][.//span[@class='text' and contains(text(),'Nested Frames')]]");
    public By modalDialogsButton = By.xpath("//*[@id='item-4'][.//span[@class='text' and contains(text(),'Modal Dialogs')]]");

    public AlertsFrameWindowsPage(WebDriver driver) {
        super(driver);
    }

    //actions
    public void clickBrowserWindowsButton () {driver.findElement(browserWindowsButton).click();}
    public void clickAlertsButton () {driver.findElement(alertsButton).click();}
    public void clickFramesButton () {driver.findElement(framesButton).click();}
    public void clickNestedFramesButton () {driver.findElement(nestedFramesButton).click();}
    public void clickModalDialogsButton () {driver.findElement(modalDialogsButton).click();}

}
