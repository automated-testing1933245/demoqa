package poms.formspages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import poms.BasePage;

public class FormsPage extends BasePage {

    public static final String url = "https://demoqa.com/forms";
    public static final String title = "Forms";


    //selectors
    public By practiceFormButton = By.xpath("//*[@id='item-0'][.//span[@class='text' and contains(text(),'Practice Form')]]");

    public FormsPage(WebDriver driver) {
        super(driver);
    }

    // actions
    public void clickPracticeFormButton() {driver.findElement(practiceFormButton).click();}
}
