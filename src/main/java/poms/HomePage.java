package poms;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class HomePage {

    private WebDriver driver;

    public static final String url = "https://demoqa.com/";

    // selectors

    public By elementsButton = By.xpath("//*[@class='card mt-4 top-card'][.//h5[contains(text(),'Elements')]]");
    public By formsButton = By.xpath("//*[@class='card mt-4 top-card'][.//h5[contains(text(),'Forms')]]");
    public By alertsWindowsButton = By.xpath("//*[@class='card mt-4 top-card'][.//h5[contains(text(),'Alerts, Frame & Windows')]]");
    public By widgetsButton = By.xpath("//*[@class='card mt-4 top-card'][.//h5[contains(text(),'Widgets')]]");
    public By interactionsButton = By.xpath("//*[@class='card mt-4 top-card'][.//h5[contains(text(),'Interactions')]]");
    public By bookStoreApplicationButton = By.xpath("//*[@class='card mt-4 top-card'][.//h5[contains(text(),'Book Store Application')]]");

    // constructor
    public HomePage(WebDriver driver) {
        this.driver = driver;
    }

    // actions
    public void clickElementsButton() {driver.findElement(elementsButton).click();}
    public void clickFormsButton() {driver.findElement(formsButton).click();}
    public void clickAlertsFramesAndWindowsButton() {driver.findElement(alertsWindowsButton).click();}
    public void clickWidgetsButton() {driver.findElement(widgetsButton).click();}
    public void clickInteractionsButton() {driver.findElement(interactionsButton).click();}
    public void clickBookStoreApplicationButton() {driver.findElement(bookStoreApplicationButton).click();}

}
