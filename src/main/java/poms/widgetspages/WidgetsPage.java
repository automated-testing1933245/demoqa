package poms.widgetspages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import poms.BasePage;

public class WidgetsPage extends BasePage {

    public static String url = "https://demoqa.com/widgets";
    public static final String title = "Widgets";

    //selectors
    public By accordianButton = By.xpath("//*[@id='item-0'][.//span[@class='text' and contains(text(),'Accordian')]]");
    public By autoCompleteButton = By.xpath("//*[@id='item-1'][.//span[@class='text' and contains(text(),'Auto Complete')]]");
    public By datePickerButton = By.xpath("//*[@id='item-2'][.//span[@class='text' and contains(text(),'Date Picker')]]");
    public By sliderButton = By.xpath("//*[@id='item-3'][.//span[@class='text' and contains(text(),'Slider')]]");
    public By progressBarButton = By.xpath("//*[@id='item-4'][.//span[@class='text' and contains(text(),'Progress Bar')]]");
    public By tabsButton = By.xpath("//*[@id='item-5'][.//span[@class='text' and contains(text(),'Tabs')]]");
    public By toolTipsButton = By.xpath("//*[@id='item-6'][.//span[@class='text' and contains(text(),'Tool Tips')]]");
    public By menuButton = By.xpath("//*[@id='item-7'][.//span[@class='text' and contains(text(),'Menu')]]");
    public By selectMenuButton = By.xpath("//*[@id='item-8'][.//span[@class='text' and contains(text(),'Select Menu')]]");

    public WidgetsPage(WebDriver driver) {
        super(driver);
    }

    // actions
    public void clickAccordianButton () {driver.findElement(accordianButton).click();}
    public void clickAutoCompleteButton() {driver.findElement(autoCompleteButton).click();}
    public void clickDatePickerButton() {driver.findElement(datePickerButton).click();}
    public void clickSliderButton() {driver.findElement(sliderButton).click();}
    public void clickProgressBarButton() {driver.findElement(progressBarButton).click();}
    public void clickTabsButton() {driver.findElement(tabsButton).click();}
    public void clickToolTipsButton() {driver.findElement(toolTipsButton).click();}
    public void clickMenuButton() {driver.findElement(menuButton).click();}
    public void clickSelectMenuButton() {driver.findElement(selectMenuButton).click();}
}
