package poms.bookstorepages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import poms.BasePage;

import java.util.List;

public class BookStorePage extends BasePage {

    public static String url = "https://demoqa.com/books";
    public static final String title = "Book Store";


    //selectors
    public By bookStoreApplicationDropdown = By.xpath("//*[@class = 'element-list collapse show']");
    public By tableLoginButton = By.xpath("//*[@id='login' and contains(text(),'Login')]");
    public By bookStoreButton = By.xpath("//*[@id='item-2'][.//span[@class='text' and contains(text(),'Book Store')]]");
    public By profileButton = By.xpath("//*[@id='item-3'][.//span[@class='text' and contains(text(),'Profile')]]");
    public By bookStoreApiButton = By.xpath("//*[@id='item-4'][.//span[@class='text' and contains(text(),'Book Store API')]]");

    // constructors
    public BookStorePage(WebDriver driver) { super(driver); }

    //actions
    public void clickTableLoginButton() {driver.findElement(tableLoginButton).click();}
    public void clickBookStoreButton() {driver.findElement(bookStoreButton).click();}
    public void clickProfileButton() {driver.findElement(profileButton).click();}
    public void clickBookStoreApiButton() {driver.findElement(bookStoreApiButton).click();}

    //verifications
    public boolean bookStoreApplicationDropdownIsDisplayed() { return driver.findElement(bookStoreApplicationDropdown).isDisplayed(); }
    public boolean logoutButtonCannotBeFound() {
        List<WebElement> logoutButtons = driver.findElements(By.xpath("//*[@id='submit' and contains(text(),'Log out')]"));

        if (logoutButtons.isEmpty()) {
            System.out.println("Logout button is not found (as expected)");
            return true;
        } else {
            return false;
        }
    }

}
