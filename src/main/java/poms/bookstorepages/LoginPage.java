package poms.bookstorepages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import poms.BasePage;

public class LoginPage extends BasePage {

    public static String url = "https://demoqa.com/login";
    public final String title = "Login";

    // constructor
    public LoginPage(WebDriver driver) { super(driver); }

    // selectors
    public By usernameInput = By.cssSelector("#userName");
    public By passwordInput = By.cssSelector("#password");
    public By loginButton = By.xpath("//*[@id='login']");
    public By newUserButton = By.cssSelector("#newUser");
    public By usernameInputIsEmpty = By.cssSelector("#userName.mr-sm-2.is-invalid.form-control");
    public By passwordInputIsEmpty = By.cssSelector("#password.mr-sm-2.is-invalid.form-control");
    public By errorMessageParagraph = By.xpath("//*[@id='name' and contains(text(),'Invalid username or password!')]");


    // actions
    public void enterUsername(String username) {
        driver.findElement(usernameInput).sendKeys(username);
    }
    public void enterPassword(String password) {
        driver.findElement(passwordInput).sendKeys(password);
    }
    public void clickLoginButton() {
        driver.findElement(loginButton).click();
    }
    public void clickNewUserButton() {driver.findElement(newUserButton).click();}

    //enter credential and login
    public void enterCredentialsAndLogin(String username, String password) {
        enterUsername(username);
        enterPassword(password);
        clickLoginButton();
    }
    public void enterUsernameAndClickLogin(String username) {
        enterUsername(username);
        clickLoginButton();
    }
    public void enterPasswordAndClickLogin(String password) {
        enterPassword(password);
        clickLoginButton();
    }

    // verifications
    public boolean errorMessageIsDisplayed() { return driver.findElement(errorMessageParagraph).isDisplayed(); }
    public boolean usernameInputIsEmptyIsDisplayed(){ return driver.findElement(usernameInputIsEmpty).isDisplayed();}
    public boolean passwordInputIsEmptyIsDisplayed(){ return driver.findElement(passwordInputIsEmpty).isDisplayed();}

}

