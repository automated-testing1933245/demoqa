package poms.bookstorepages;

import models.UserInfo;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import poms.BasePage;

public class RegisterPage extends BasePage {

    public static String url = "https://demoqa.com/register";
    public final String title = "Register";

    public String reCaptchaIFrameID = "reCAPTCHA";

    //selectors
    public By firstNameInput = By.cssSelector("#firstname");
    public By lastNameInput = By.cssSelector("#lastname");
    public By userNameInput = By.cssSelector("#userName");
    public By passwordInput = By.cssSelector("#password");
    public By reCaptchaCheckbox = By.cssSelector("#recaptcha-anchor");
    public By reCaptchaIFrame = By.xpath("//*[@id='g-recaptcha']/div/div/iframe[@title='reCAPTCHA']");
    public By registerButton = By.cssSelector("#register");
    public By backToLoginButton = By.cssSelector("#gotologin");
    //public Alert alert;

    //constructors
    public RegisterPage(WebDriver driver) {
        super(driver);
    }

    public void register(UserInfo userInfo) {
        enterFirstName(userInfo.getFirstName());
        enterLastName(userInfo.getLastName());
        enterUserName(userInfo.getUserName());
        enterPassword(userInfo.getPassword());
    }

    //actions
    public void enterFirstName (String firstName) {driver.findElement(this.firstNameInput).sendKeys(firstName);}
    public void enterLastName (String lastName) {driver.findElement(this.lastNameInput).sendKeys(lastName);}
    public void enterUserName (String userName) {driver.findElement(this.userNameInput).sendKeys(userName);}
    public void enterPassword (String password) {driver.findElement(this.passwordInput).sendKeys(password);}
    public void clickRegisterButton() {driver.findElement(registerButton).click();}
    public void clickBackToLoginButton() {driver.findElement(backToLoginButton).click();}
    public void checkCaptchaCheckbox() {driver.findElement(reCaptchaCheckbox).click();}
    public void switchToReCaptchaIFrame() {
        wait.until(ExpectedConditions.frameToBeAvailableAndSwitchToIt(reCaptchaIFrame));
        //driver.switchTo().frame(driver.findElement(reCaptchaIFrame));
    }
    public void switchToUserRegisterSuccessfullyAlert(){driver.switchTo().alert();}

    //verifications
/*
    public String userRegisterSuccefullyAlertText (){
        switchToUserRegisterSuccessfullyAlert();
        return alert.getText();
    }
*/
}
