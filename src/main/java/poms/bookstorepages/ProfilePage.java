package poms.bookstorepages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import poms.BasePage;

public class ProfilePage extends BasePage {

    public static String url = "https://demoqa.com/profile";
    public final String title = "Profile";

    //selectors
    public By notLoggedInLabel = By.cssSelector("#notLoggin-label");
    public By loginButton = By.xpath("//*[@id='notLoggin-label']/a[@href='/login']");
    public By registerButton = By.xpath("//*[@id='notLoggin-label']/a[@href='/register']");
    public By deleteAccountButton = By.xpath("//*[@id='submit' and contains(text(),'Delete Account')]");
    public By deleteAllBooksButton = By.xpath("//*[@class='text-right button di']//button[@id='submit' and contains(text(), 'Delete All Books')]");
    public By goToBookStoreButton = By.xpath("//*[@id='gotoStore' and contains(text(),'Go To Book Store')]");

    // constructors
    public ProfilePage(WebDriver driver) { super(driver); }

    //actions
    public void clickLoginButton() { driver.findElement(loginButton).click();}
    public void clickRegisterButton() { driver.findElement(registerButton).click();}
    public void clickLogoutButton() { driver.findElement(logoutButton).click();}
    public void clickDeleteAccountButton() { driver.findElement(deleteAccountButton).click();}
    public void clickDeleteAllBooksButton() { driver.findElement(deleteAllBooksButton).click();}
    public void clickGoToBookStoreButton() { driver.findElement(goToBookStoreButton).click();}

    //verifications
}
