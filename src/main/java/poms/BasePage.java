package poms;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Duration;

public abstract class BasePage {
    protected WebDriver driver;
    public WebDriverWait wait;

    //selectors
    public By title = By.xpath("//*[@class='main-header']");
    public By loggedInUserName = By.xpath("//*[@id='userName-value']");
    public By logoutButton = By.xpath("//*[@id='submit' and contains(text(),'Log out')]");
    public By dropDownLoginButton = By.id("item-0");

    //constructor
    public BasePage(WebDriver driver) {
        this.driver = driver;
        wait = new WebDriverWait(driver, Duration.ofSeconds(120));
    }

    //actions
    public void clickDropDownLoginButton() {driver.findElements(dropDownLoginButton).get(5).click();}

    public void refresh() { driver.navigate().refresh();}

    //verifications
    public boolean logOutButtonIsDisplayed() { return driver.findElement(logoutButton).isDisplayed(); }
    public String getPageTitle() { return driver.findElement(title).getText(); }
    public String getLoggedInUserName() {
        wait.until(ExpectedConditions.visibilityOfElementLocated(loggedInUserName));
        return driver.findElement(loggedInUserName).getText(); }


}

