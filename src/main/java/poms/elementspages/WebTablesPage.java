package poms.elementspages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import poms.BasePage;

public class WebTablesPage extends BasePage {

    public static final String url = "https://demoqa.com/webtables";
    public static final String title = "Web Tables";

    //constructor
    public WebTablesPage(WebDriver driver) { super(driver); }

    //selectors
    public By addButton = By.id("addNewRecordButton");
    public By registrationForm = By.id("userForm");

    public By firstName = By.id("firstName");
    public By lastName = By.id("lastName");
    public By email = By.id("userEmail");
    public By age = By.id("age");
    public By salary = By.id("salary");
    public By department = By.id("department");
    public By submitButton = By.id("submit");

    //actions
    public void clickAddButton (){
        driver.findElement(addButton).click();
    }

    public void clickSubmitButton (){
        driver.findElement(submitButton).click();
    }

    //validations
    public boolean registrationFormIsDisplayed (){
        return driver.findElement(registrationForm).isDisplayed();
    }


}
