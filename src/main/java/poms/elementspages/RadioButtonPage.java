package poms.elementspages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import poms.BasePage;

public class RadioButtonPage extends BasePage {

    public static final String url = "https://demoqa.com/radio-button";
    public static final String title = "Radio Button";

    //constructor
    public RadioButtonPage(WebDriver driver) {
        super(driver);
    }

    //selectors
    public By yesRadioButton = By.cssSelector("label[for='yesRadio']");
    public By impressiveRadioButton = By.cssSelector("label[for='impressiveRadio']");
    public By noRadioButton = By.id("noRadio");
    public By textSuccess = By.cssSelector(".text-success");

    //actions
    public void clickOnRadioButton(String radioButton) {
        if (radioButton.equals("Yes")) {
            driver.findElement(yesRadioButton).click();
        } else if (radioButton.equals("Impressive")) {
            driver.findElement(impressiveRadioButton).click();
        }

    }

    //verifications
    public boolean radioButtonIsSelected(String radioButton) {
        if (radioButton.equals("Yes")) {
            driver.findElement(yesRadioButton).isSelected();
            return true;
        } else if (radioButton.equals("Impressive")) {
            driver.findElement(impressiveRadioButton).isSelected();
            return true;
        }
        return false;
    }

    public boolean radioButtonTextIsDisplayed(String radioButton) {
        if (driver.findElement(textSuccess).getText().equals(radioButton)) {
            System.out.println(driver.findElement(textSuccess).getText());
            return true;
        } else return false;
    }

    public boolean isNoRadioButtonDisabled (){
        return driver.findElement(noRadioButton).isEnabled();
    }
}
