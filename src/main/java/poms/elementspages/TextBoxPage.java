package poms.elementspages;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import poms.BasePage;

public class TextBoxPage extends BasePage {

    public static final String url = "https://demoqa.com/text-box";
    public static final String title = "Text Box";

    //constructor
    public TextBoxPage(WebDriver driver) {
        super(driver);
    }

    //selectors
    public By fullNameInput = By.id("userName");
    public By emailInput = By.id("userEmail");
    public By currentAddressInput =  By.id("currentAddress");
    public By permanentAddressInput = By.id("permanentAddress");
    public By submitButton = By.id("submit");
    public By outputArea = By.id("output");
    public By outputText = By.cssSelector("div.col-md-12.col-sm-12");
    public By outputName = By.id("name");
    public By outputEmail = By.id("email");
    public By outputCurrentAddress = By.cssSelector("p#currentAddress.mb-1");
    public By outputPermanentAddress = By.cssSelector("p#permanentAddress.mb-1");

    //actions
    public void enterFullName(String fullName)  {
        driver.findElement(fullNameInput).sendKeys(fullName);
    }
    public void enterEmail(String email)  {
        driver.findElement(emailInput).sendKeys(email);
    }

    public void enterCurrentAddress(String currentAddress)  {
        driver.findElement(currentAddressInput).sendKeys(currentAddress);
    }

    public void enterPermanentAddress(String permanentAddress)  {
        driver.findElement(permanentAddressInput).sendKeys(permanentAddress);
    }

    public void clickSubmitButton(){driver.findElement(submitButton).click();}

    public void hoverCursorOverEmailTextBoxInput(){
        Actions actions = new Actions(driver);

        actions.moveToElement(driver.findElement(emailInput)).perform();
        try {
            // Sleep for 1000 milliseconds (1 second)
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    // JavaScript function to get tooltip text
    public String getEmailTexboxInputTooltipText() {
        JavascriptExecutor js = (JavascriptExecutor) driver;
        // Use JavaScript to retrieve tooltip text
        return (String) js.executeScript("return arguments[0].title;", driver.findElement(By.id("userEmail")));
    }
    public String getOutputText (){ return driver.findElement(outputArea).getText(); }

    //verifications
    public boolean outputTextIsEmpty (){
        WebElement outputTextElement = driver.findElement(outputText);
        return outputTextElement.findElements(By.xpath("./*")).isEmpty();
    }
    public String getOutputName (){ return driver.findElement(outputName).getText(); }
    public String getOutputEmail (){ return driver.findElement(outputEmail).getText(); }
    public String getOutputCurrentAddress (){ return driver.findElement(outputCurrentAddress).getText(); }
    public String getOutputPermanentAddress (){ return driver.findElement(outputPermanentAddress).getText(); }

}
