package poms.elementspages;


import org.openqa.selenium.WebDriver;
import poms.BasePage;

public class DynamicPropertiesPage extends BasePage {

    public static final String url = "https://demoqa.com/dynamic-properties";
    public static final String title = "Dynamic Properties";


    //constructor
    public DynamicPropertiesPage(WebDriver driver) { super(driver); }
}
