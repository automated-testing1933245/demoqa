package poms.elementspages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import poms.BasePage;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CheckBoxPage extends BasePage {

    public static final String url = "https://demoqa.com/checkbox";
    public static final String title = "Check Box";

    //constructor
    public CheckBoxPage(WebDriver driver) {
        super(driver);
    }

    //selectors
    public Map<String, By> checkboxesMap = new HashMap<>(){
        {
            put("homeCheckbox", By.xpath("//label[@for='tree-node-home']"));
            put("desktopCheckbox", By.xpath("//label[@for='tree-node-desktop']"));
            put("notesCheckbox", By.xpath("//label[@for='tree-node-notes']"));
            put("commandsCheckbox", By.xpath("//label[@for='tree-node-commands']"));
            put("documentsCheckbox", By.xpath("//label[@for='tree-node-documents']"));
            put("workSpaceCheckbox", By.xpath("//label[@for='tree-node-workspace']"));
            put("reactCheckbox", By.xpath("//label[@for='tree-node-react']"));
            put("angularCheckbox", By.xpath("//label[@for='tree-node-angular']"));
            put("veuCheckbox", By.xpath("//label[@for='tree-node-veu']"));
            put("officeCheckbox", By.xpath("//label[@for='tree-node-office']"));
            put("publicCheckbox", By.xpath("//label[@for='tree-node-public']"));
            put("privateCheckbox", By.xpath("//label[@for='tree-node-private']"));
            put("classifiedCheckbox", By.xpath("//label[@for='tree-node-classified']"));
            put("generalCheckbox", By.xpath("//label[@for='tree-node-general']"));
            put("downloadsCheckbox", By.xpath("//label[@for='tree-node-downloads']"));
            put("wordFileDocCheckbox", By.xpath("//label[@for='tree-node-wordFile']"));
            put("excelFileDocCheckbox", By.xpath("//label[@for='tree-node-excelFile']"));
        }
    };

    Map<String, By> checkboxesResultsMap = new HashMap<>(){
        {
            put("homeResult", By.xpath("//span[contains(text(),'home')]"));
            put("desktopResult", By.xpath("//span[contains(text(),'desktop')]"));
            put("notesResult", By.xpath("//span[contains(text(),'notes')]"));
            put("commandsResult", By.xpath("//span[contains(text(),'commands')]"));
            put("documentsResult", By.xpath("//span[contains(text(),'documents')]"));
            put("workSpaceResult", By.xpath("//span[contains(text(),'workspace')]"));
            put("reactResult", By.xpath("//span[contains(text(),'react')]"));
            put("angularResult", By.xpath("//span[contains(text(),'angular')]"));
            put("veuResult", By.xpath("//span[contains(text(),'veu')]"));
            put("officeResult", By.xpath("//span[contains(text(),'office')]"));
            put("publicResult", By.xpath("//span[contains(text(),'public')]"));
            put("privateResult", By.xpath("//span[contains(text(),'private')]"));
            put("classifiedText", By.xpath("//span[contains(text(),'classified')]"));
            put("generalResult", By.xpath("//span[contains(text(),'general')]"));
            put("downloadsResult", By.xpath("//span[contains(text(),'downloads')]"));
            put("wordFileResult", By.xpath("//span[contains(text(),'wordFile')]"));
            put("excelFileResult", By.xpath("//span[contains(text(),'excelFile')]"));
        }
    };

    public By expandAllButton = By.xpath("//button[@title='Expand all']");
    public By collapseAllButton = By.xpath("//button[@title='Collapse all']");
    public By checkboxTree = By.xpath("//*[@id='tree-node']/ol/li");

    //actions
    public void clickOnHomeCheckbox(){ driver.findElement(checkboxesMap.get("homeCheckbox")).click(); }
    public void clickOnGeneralCheckbox(){ driver.findElement(checkboxesMap.get("generalCheckbox")).click(); }
    public void clickOnExpandAllButton(){ driver.findElement(expandAllButton).click(); }
    public void clickOnCollapseAllButton(){ driver.findElement(collapseAllButton).click(); }

    //verifications
    public boolean homeCheckboxIsSelected(){ return driver.findElement(By.id("tree-node-home")).isSelected(); }
    public boolean generalCheckboxIsSelected(){ return driver.findElement(By.id("tree-node-general")).isSelected(); }
    public boolean homeResultIsDisplayed() { return driver.findElement(checkboxesResultsMap.get("homeResult")).isDisplayed(); }
    public boolean resultCheckboxesContain(String result) {
        List<WebElement> childElements = driver.findElement(By.id("result")).findElements(By.xpath(".//*"));

            for (WebElement child : childElements) {
                System.out.println(child.getText());
                if (result.equals(child.getText())){
                    return true;
                }
            }
            return false;
    }

    public boolean allCheckboxesTreeIsExpanded(){
        try {
            // Look for child elements that are visible when the tree is expanded
            List<WebElement> childNodes = driver.findElements(By.cssSelector("span.rct-text"));
            return childNodes.size() == 17;

        } catch (org.openqa.selenium.NoSuchElementException e) {
            // Handle the case where the element does not exist or is not visible
            return false;
        }
    }
}
