package poms.elementspages;

import org.openqa.selenium.WebDriver;
import poms.BasePage;

public class LinksPage extends BasePage {

    public static final String url = "https://demoqa.com/links";
    public static final String title = "Links";

    //constructor
    public LinksPage(WebDriver driver) { super(driver); }
}
