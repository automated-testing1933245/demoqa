package poms.elementspages;

import org.openqa.selenium.WebDriver;
import poms.BasePage;

public class UploadDownloadPage extends BasePage {

    public static final String url = "https://demoqa.com/upload-download";
    public static final String title = "Upload and Download";

    //constructor
    public UploadDownloadPage(WebDriver driver) { super(driver); }
}
