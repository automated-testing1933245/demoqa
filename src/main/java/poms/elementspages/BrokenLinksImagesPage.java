package poms.elementspages;

import org.openqa.selenium.WebDriver;
import poms.BasePage;

public class BrokenLinksImagesPage extends BasePage {

    public static final String url = "https://demoqa.com/broken";
    public static final String title = "Broken Links - Images";

    //constructor
    public BrokenLinksImagesPage(WebDriver driver) { super(driver); }
}
