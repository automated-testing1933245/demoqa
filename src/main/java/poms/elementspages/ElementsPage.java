package poms.elementspages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import poms.BasePage;

public class ElementsPage extends BasePage {

    public static final String url = "https://demoqa.com/elements";
    public static final String title = "Elements";

    //constructor
    public ElementsPage(WebDriver driver) { super(driver); }

    //selectors
    public By textBoxButton = By.xpath("//*[@id='item-0'][.//span[@class='text' and contains(text(),'Text Box')]]");
    public By checkBoxButton = By.xpath("//*[@id='item-1'][.//span[@class='text' and contains(text(),'Check Box')]]");
    public By radioButton = By.xpath("//*[@id='item-2'][.//span[@class='text' and contains(text(),'Radio Button')]]");
    public By webTablesButton = By.xpath("//*[@id='item-3'][.//span[@class='text' and contains(text(),'Web Tables')]]");
    public By buttonsButton = By.xpath("//*[@id='item-4'][.//span[@class='text' and contains(text(),'Buttons')]]");
    public By linksButton = By.xpath("//*[@id='item-5'][.//span[@class='text' and contains(text(),'Links')]]");
    public By brokenLinksImagesButton = By.xpath("//*[@id='item-6'][.//span[@class='text' and contains(text(),'Broken Links - Images')]]");
    public By uploadAndDownloadButton = By.xpath("//*[@id='item-7'][.//span[@class='text' and contains(text(),'Upload and Download')]]");
    public By dynamicPropertiesButton = By.xpath("//*[@id='item-8'][.//span[@class='text' and contains(text(),'Dynamic Properties')]]");

    //actions
    public void clickTextBoxButton (){driver.findElement(textBoxButton).click();}
    public void clickCheckBoxButton (){driver.findElement(checkBoxButton).click();}
    public void clickRadioButton (){driver.findElement(radioButton).click();}
    public void clickWebTablesButton (){driver.findElement(webTablesButton).click();}
    public void clickButtonsButton (){driver.findElement(buttonsButton).click();}
    public void clickLinksButton (){driver.findElement(linksButton).click();}
    public void clickBrokenLinksImagesButton (){driver.findElement(brokenLinksImagesButton).click();}
    public void clickUploadAndDownloadButton (){driver.findElement(uploadAndDownloadButton).click();}
    public void clickDynamicPropertiesButton (){driver.findElement(dynamicPropertiesButton).click();}
}
