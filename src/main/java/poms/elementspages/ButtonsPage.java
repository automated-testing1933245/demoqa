package poms.elementspages;

import org.openqa.selenium.WebDriver;
import poms.BasePage;

public class ButtonsPage extends BasePage {

    public static final String url = "https://demoqa.com/buttons";
    public static final String title = "Buttons";

    //constructor
    public ButtonsPage(WebDriver driver) { super(driver); }

}
