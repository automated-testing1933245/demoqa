package poms.interactionspages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import poms.BasePage;

public class InteractionsPage extends BasePage {

    public static String url = "https://demoqa.com/interaction";
    public static final String title = "Interactions";

    //selectors
    public By sortableButton = By.xpath("//*[@id='item-0'][.//span[@class='text' and contains(text(),'Sortable')]]");
    public By selectableButton = By.xpath("//*[@id='item-1'][.//span[@class='text' and contains(text(),'Selectable')]]");
    public By resizableButton = By.xpath("//*[@id='item-2'][.//span[@class='text' and contains(text(),'Resizable')]]");
    public By droppableButton = By.xpath("//*[@id='item-3'][.//span[@class='text' and contains(text(),'Droppable')]]");
    public By draggableButton = By.xpath("//*[@id='item-4'][.//span[@class='text' and contains(text(),'Dragabble')]]");

    public InteractionsPage(WebDriver driver) {
        super(driver);
    }

    // actions
    public void clickSortableButton() {driver.findElement(sortableButton).click();}
    public void clickSelectableButton() {driver.findElement(selectableButton).click();}
    public void clickResizableButton() {driver.findElement(resizableButton).click();}
    public void clickDroppableButton() {driver.findElement(droppableButton).click();}
    public void clickDraggableButton() {driver.findElement(draggableButton).click();}
}


